
import java.io.IOException;
import java.util.Scanner;

public class HangmanApplication {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("\n" +
                "\n" +
                " ________  ________  _________  _______   ________   ________  _______      \n" +
                "|\\   __  \\|\\   __  \\|\\___   ___\\\\  ___ \\ |\\   ___  \\|\\   ____\\|\\  ___ \\     \n" +
                "\\ \\  \\|\\  \\ \\  \\|\\  \\|___ \\  \\_\\ \\   __/|\\ \\  \\\\ \\  \\ \\  \\___|\\ \\   __/|    \n" +
                " \\ \\   ____\\ \\  \\\\\\  \\   \\ \\  \\ \\ \\  \\_|/_\\ \\  \\\\ \\  \\ \\  \\    \\ \\  \\_|/__  \n" +
                "  \\ \\  \\___|\\ \\  \\\\\\  \\   \\ \\  \\ \\ \\  \\_|\\ \\ \\  \\\\ \\  \\ \\  \\____\\ \\  \\_|\\ \\ \n" +
                "   \\ \\__\\    \\ \\_______\\   \\ \\__\\ \\ \\_______\\ \\__\\\\ \\__\\ \\_______\\ \\_______\\\n" +
                "    \\|__|     \\|_______|    \\|__|  \\|_______|\\|__| \\|__|\\|_______|\\|_______|\n" +
                "                                                                            \n" +
                "                                                                            \n" +
                "                                                                            \n" +
                "\n");
        System.out.println();


        System.out.println("Saisissez le niveau de difficulté :" +
                "\n (LEVEL 1) : vous avez le droit à la première et la dernière lettre du mot à deviner pour vous aider"+
                "\n (LEVEL 2) : Vous n'avez plus que la première lettre du mot affichée pour vous aider" +
                "\n (LEVEL 3) : Vous n'avez plus aucune lettre pour vous aider");

        System.out.println("Saissir 1, 2 ou 3 correspondant au LEVEL :");
        char level = (sc.next().toUpperCase()).charAt(0);

        boolean doYouWantToPlay = true;
        while (doYouWantToPlay) {
            System.out.println();
            System.out.println("Le jeux commence !");
            Hangman game = new Hangman(level);
            do {
                System.out.println();
                System.out.println(game.drawPicture());
                System.out.println();
                System.out.println(game.getFormalCurrentGuess());
                System.out.println();

                // Get the guess
                System.out.println("Entrez un caractère que vous pensez être dans le mot");
                char guess = (sc.next().toLowerCase()).charAt(0);
                System.out.println();

                // Check if the character has been guessed before
                if (game.isGuessedAlready(guess)) {
                    System.out.println("Essayez encore. Vous avez déjà deviné cette lettre");
                    guess = (sc.next().toLowerCase()).charAt(0);
                }

                if (game.playGuess(guess)) {
                    System.out.println("Bien deviné ! Cette lettre est dans le mot");
                }
                else {
                    System.out.println("Malheureusement, ce mot n'est pas dans le mot mystère");
                }
            }
            while (!game.gameOver());

            System.out.println();
            System.out.println("Relancer une partie de la potence ? . Taper Y pour relancer une partie.");
            Character response = (sc.next().toUpperCase()).charAt(0);
            doYouWantToPlay = (response == 'Y');
        }

    }
}
