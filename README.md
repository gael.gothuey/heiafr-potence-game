```asciiarmor
 ____        __                                   
/\  _`\     /\ \__                                
\ \ \L\ \___\ \ ,_\    __    ___     ___     __   
 \ \ ,__/ __`\ \ \/  /'__`\/' _ `\  /'___\ /'__`\ 
  \ \ \/\ \L\ \ \ \_/\  __//\ \/\ \/\ \__//\  __/ 
   \ \_\ \____/\ \__\ \____\ \_\ \_\ \____\ \____\
    \/_/\/___/  \/__/\/____/\/_/\/_/\/____/\/____/                     
```



Chloé Amez-Droz
Julien Beney
Gaël Gothuey

## Spécifications 

Le jeu du pendu est un célèbre jeu de mots dont le but est de deviner un mot ou une expression en proposant des lettres. Mais vous avez droit à un nombre limité d'erreur ! Et chaque nouvelle erreur vous rapproche de la potence ! Les règles sont simples : si vous atteignez la limite des erreurs, vous avez perdu. Si vous trouvez le mot avant d'atteindre la limite, vous avez gagné.

 

**Les niveaux de difficultés du jeu :** 

 

Lorsque vous commencez le jeu, on vous propose de choisir parmi les trois niveaux disponibles.

 

Le **niveau 1** est le niveau débutant : vous avez le droit à la première et la dernière lettre du mot à deviner pour vous aider.

 

Le **niveau 2** est le niveau intermédiaire : Vous n'avez plus que la première lettre du mot affichée pour vous aider.

 

Le **niveau 3** est le niveau expert : Vous n'avez plus aucune lettre pour vous aider.

 

**Déroulement du jeu :** 

 

Via le menu d’introduction, il faut choisir parmi les 3 niveaux de difficultés. Par défaut, le niveau 2 soit intermédiaire est sélectionné. Un mot est choisi au hasard dans le dictionnaire puis le joueur doit devenir celui-ci. Les lettres masquées sont indiquées par des under score « _ ». 

 

Si la lettre proposée fait partie une ou plusieurs fois du mot à deviner, alors la lettre apparait dans le compartiment "mot à deviner" . La lettre ne pourra plus être rejouée.

 

Si la lettre proposée ne fait pas partie du mot à deviner, le joueur perd l'une de des 10 chances de trouver le mot mystère. Dans la partie "nombre d'erreurs" doit apparaître une partie de la potence du futur pendu !

Le joueur gagne le jeu s’il trouve le mot mystère en moins de 10 erreurs et il échappe alors à la potence ! 